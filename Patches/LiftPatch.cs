﻿using Harmony;

namespace SameThings.Patches {
	[HarmonyPatch(typeof(Lift), nameof(Lift.UseLift))]
	public class LiftPatch {
		public static void Prefix(Lift __instance) {
			if (Configs.LiftMoveDuration == -1) {
				return;
			}

			__instance.movingSpeed = Configs.LiftMoveDuration;
		}
	}
}