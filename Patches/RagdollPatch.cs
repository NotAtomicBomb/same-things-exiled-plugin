﻿using Harmony;

namespace SameThings.Patches {
	[HarmonyPatch(typeof(Ragdoll), nameof(Ragdoll.Start))]
	public class RagdollPatch {
		public static void Prefix(Ragdoll __instance) {
			if (Configs.RagdollAutoCleanup > 0) {
				State.Ragdolls.Add(__instance, State.CleanupTime + Configs.RagdollAutoCleanup);
			}
		}
	}
}