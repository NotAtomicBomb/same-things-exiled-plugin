﻿using Harmony;

namespace SameThings.Patches {
	[HarmonyPatch(typeof(BreakableWindow), nameof(BreakableWindow.ServerDamageWindow))]
	public class BreakableWindowPatch {
		public static void Prefix(BreakableWindow __instance) {
			if (Configs.WindowHealth < 1 || State.BreakableWindows.Contains(__instance)) {
				return;
			}

			__instance.health = Configs.WindowHealth == 0 ? 9999999f : Configs.WindowHealth;
			State.BreakableWindows.Add(__instance);
		}
	}
}