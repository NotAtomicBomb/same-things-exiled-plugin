﻿using Harmony;

namespace SameThings.Patches {
	[HarmonyPatch(typeof(Radio), nameof(Radio.UseBattery))]
	public class RadioPatch {
		public static bool Prefix() {
			return !Configs.UnlimitedRadio;
		}
	}
}