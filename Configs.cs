using static EXILED.Plugin;
using System.Collections.Generic;

namespace SameThings {
	internal static class Configs {
		internal static bool PluginOn;

		internal static float WindowHealth;
		internal static int ForceRestart;

		internal static bool WarheadCleanup;
		internal static int ItemAutoCleanup;
		internal static List<int> ItemCleanupIgnore;
		internal static int RagdollAutoCleanup;
		internal static List<string> NicknameFilter;
		internal static string NicknameFilterReason;
		internal static List<int> TeslaTriggerableTeam;
		internal static bool ReduceAmmo;
		internal static bool UnlimitedRadio;
		internal static float LiftMoveDuration;

		internal static int AutoWarheadStart;
		internal static bool AutoWarheadLock;
		internal static string AutoWarheadStartText;

		internal static float DecontaminationTime;

		internal static int GeneratorDuration;
		internal static List<string> GeneratorKeycardPerm;
		internal static List<int> GeneratorInsertTeams;
		internal static List<int> GeneratorEjectTeams;
		internal static List<int> GeneratorUnlockTeams;

		internal static Dictionary<int, int> MaxHealth;
		internal static Dictionary<int, int> SelfHealingDuration;
		internal static Dictionary<int, int> SelfHealingAmount;

		internal static int Scp106LureAmount;
		internal static float Scp106LureReload;
		internal static List<int> Scp106LureTeam;
		internal static List<int> NoInfAmmoTeam;

		internal static void Reload() {
			PluginOn = Config.GetBool("same_things_on", true);

			WindowHealth = Config.GetFloat("window_health");
			ForceRestart = Config.GetInt("seconds_until_force_restart");

			WarheadCleanup = Config.GetBool("nuke_cleanup");
			ItemAutoCleanup = Config.GetInt("item_cleanup");
			ItemCleanupIgnore = Config.GetIntList("cleanup_ignored_items");
			RagdollAutoCleanup = Config.GetInt("ragdoll_cleanup");
			NicknameFilter = Config.GetStringList("nickname_filter");
			NicknameFilterReason = Config.GetString("nickname_filter_reason", "You have been kicked from the server.");

			TeslaTriggerableTeam = Config.GetIntList("tesla_triggerable_team");

			ReduceAmmo = Config.GetBool("reduce_ammo_on_reload", true);
			UnlimitedRadio = Config.GetBool("unlimited_radio_battery");
			LiftMoveDuration = Config.GetFloat("lift_move_duration", -1);

			AutoWarheadStart = Config.GetInt("auto_warhead_start");
			AutoWarheadLock = Config.GetBool("auto_warhead_start_lock");
			AutoWarheadStartText = Config.GetString("auto_warhead_start_text");

			DecontaminationTime = Config.GetFloat("decontamination_time");
			GeneratorDuration = Config.GetInt("generator_duration");
			GeneratorKeycardPerm = Config.GetStringList("generator_keycard_perm");

			GeneratorInsertTeams = Config.GetIntList("generator_insert_teams");
			GeneratorEjectTeams = Config.GetIntList("generator_eject_teams");
			GeneratorUnlockTeams = Config.GetIntList("generator_unlock_teams");
			
			MaxHealth = GetIntDictionary("roles_max_health");
			SelfHealingDuration = GetIntDictionary("roles_healing_duration");
			SelfHealingAmount = GetIntDictionary("roles_healing_amount");
			
			Scp106LureAmount = Config.GetInt("scp106_lure_amount", -1);
			Scp106LureTeam = Config.GetIntList("scp106_lure_team");
			Scp106LureReload = Config.GetFloat("scp106_lure_reload");
			NoInfAmmoTeam = Plugin.Config.GetIntList("noinf_ammo_team");
		}

		private static Dictionary<int, int> GetIntDictionary(string config, Dictionary<int, int> def = null) {
			var dict = Config.GetStringDictionary(config);

			if (dict.Count == 0) {
				return def ?? new Dictionary<int, int>();
			}

			var res = new Dictionary<int, int>();
			foreach (var kv in dict) {
				if (int.TryParse(kv.Key, out var key) && int.TryParse(kv.Value, out var val)) {
					res.Add(key, val);
				}
			}

			return res;
		}
	}
}
