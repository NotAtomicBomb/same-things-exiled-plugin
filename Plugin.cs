using EXILED;
using Harmony;

namespace SameThings {
	public class SameThings : Plugin {
		public override string getName => "SameThings";
		private const string Version = "2020.0406.0";

		private HarmonyInstance _instance;

		public override void OnEnable() {
			Configs.Reload();

			if (!Configs.PluginOn) {
				Log.Info($"Plugin v{Version} disabled. Bye;");
				return;
			}

			if (EventPlugin.Version.Major == 1 && EventPlugin.Version.Minor < 9) {
				Configs.PluginOn = false;
				Log.Error($"Plugin v{Version} works only on EXILED 1.9.0+");
				return;
			}

			if (_instance == null) {
				_instance = HarmonyInstance.Create("me.nekonyx.same-things");
			}

			_instance.PatchAll();

			State.Refresh();
			EventHandlers.Subscribe();
			
			Log.Info($"Plugin v{Version} initialied");
		}

		public override void OnDisable() {
			if (!Configs.PluginOn) {
				return;
			}

			_instance.UnpatchAll();

			State.Refresh();
			EventHandlers.Unsubscribe();
		}

		public override void OnReload() { }
	}
}
