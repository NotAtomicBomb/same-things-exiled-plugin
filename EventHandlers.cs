using System;
using System.Collections.Generic;
using System.Linq;
using EXILED;
using EXILED.Extensions;
using MEC;
using Mirror;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SameThings {
	internal static class EventHandlers {
		internal static void Subscribe() {
			Events.RoundStartEvent += HandleRoundStart;
			Events.RoundEndEvent += HandleRoundEnd;
			Events.PlayerJoinEvent += HandlePlayerJoin;
			Events.TriggerTeslaEvent += HandleTeslaTrigger;
			Events.PlayerReloadEvent += HandleWeaponReload;
			Events.SetClassEvent += HandleSetClass;
			Events.ItemDroppedEvent += HandleDroppedItem;
			Events.GeneratorEjectedEvent += HandleGeneratorEject;
			Events.GeneratorInsertedEvent += HandleGeneratorInsert;
			Events.GeneratorUnlockEvent += HandleGeneratorUnlock;
			Events.FemurEnterEvent += HandleFemurEnter;
			Events.PlayerLeaveEvent += HandlePlayerLeave;
			Events.WarheadDetonationEvent += HandleWarheadDetonation;
		}

		internal static void Unsubscribe() {
			Events.RoundStartEvent -= HandleRoundStart;
			Events.RoundEndEvent -= HandleRoundEnd;
			Events.PlayerJoinEvent -= HandlePlayerJoin;
			Events.TriggerTeslaEvent -= HandleTeslaTrigger;
			Events.PlayerReloadEvent -= HandleWeaponReload;
			Events.SetClassEvent -= HandleSetClass;
			Events.ItemDroppedEvent -= HandleDroppedItem;
			Events.GeneratorEjectedEvent -= HandleGeneratorEject;
			Events.GeneratorInsertedEvent -= HandleGeneratorInsert;
			Events.GeneratorUnlockEvent -= HandleGeneratorUnlock;
			Events.FemurEnterEvent -= HandleFemurEnter;
			Events.PlayerLeaveEvent -= HandlePlayerLeave;
			Events.WarheadDetonationEvent -= HandleWarheadDetonation;
		}

		private static void HandleRoundStart() {
			if (Configs.AutoWarheadLock) {
				EventPlugin.WarheadLocked = false;
			}

			// auto restart
			if (Configs.ForceRestart > 0) {
				State.RunCoroutine(RunForceRestart());
			}

			// auto warhead
			if (Configs.AutoWarheadStart > 0) {
				State.RunCoroutine(RunAutoWarhead());
			}

			// items and ragdoll cleanup
			if (Configs.RagdollAutoCleanup > 0 || Configs.ItemAutoCleanup > 0) {
				State.RunCoroutine(RunAutoCleanup());
			}

			// setup decontamination timer
			if (Configs.DecontaminationTime > 0) {
				PlayerManager.localPlayer.GetComponent<DecontaminationLCZ>().time =
					(float) ((11.7399997711182 - Configs.DecontaminationTime) * 60.0);
			}

			// setup generators timer
			if (Configs.GeneratorDuration > 0) {
				foreach (var generator in Generator079.generators) {
					generator.startDuration = Configs.GeneratorDuration;
					generator.SetTime(Configs.GeneratorDuration);
				}
			}

			// run self healing
			if (Configs.SelfHealingDuration.Count > 0) {
				State.RunCoroutine(RunSelfHealing());
			}

			if (Configs.Scp106LureAmount == 0) {
				Object.FindObjectOfType<LureSubjectContainer>().SetState(true);
			}
		}

		private static void HandleRoundEnd() {
			State.Refresh();
		}

		private static void HandlePlayerJoin(PlayerJoinEvent ev) {
			State.AfkTime[ev.Player] = 0;
			State.PrevPos[ev.Player] = Vector3.zero;

			if (!ev.Player.serverRoles.Staff && Configs.NicknameFilter.Any(s => ev.Player.nicknameSync.MyNick.Contains(s))) {
				ServerConsole.Disconnect(ev.Player.gameObject, Configs.NicknameFilterReason);
			}
		}

		private static void HandlePlayerLeave(PlayerLeaveEvent ev) {
			if (State.AfkTime.ContainsKey(ev.Player)) {
				State.AfkTime.Remove(ev.Player);
			}

			if (State.PrevPos.ContainsKey(ev.Player)) {
				State.PrevPos.Remove(ev.Player);
			}
		}

		private static void HandleWarheadDetonation() {
			if (!Configs.WarheadCleanup) {
				return;
			}

			foreach (var pickup in Object.FindObjectsOfType<Pickup>().Where(pickup => pickup.transform.position.y < 5)) {
				NetworkServer.Destroy(pickup.gameObject);
			}

			foreach (var ragdoll in Object.FindObjectsOfType<Ragdoll>().Where(ragdoll => ragdoll.transform.position.y < 5)) {
				NetworkServer.Destroy(ragdoll.gameObject);
			}
		}

		private static void HandleTeslaTrigger(ref TriggerTeslaEvent ev) {
			if (Configs.TeslaTriggerableTeam.Count == 0) {
				return;
			}

			if (!Configs.TeslaTriggerableTeam.Contains((int) ev.Player.GetTeam())) {
				ev.Triggerable = false;
			}
		}

		private static void HandleWeaponReload(ref PlayerReloadEvent ev) {
			if (Configs.ReduceAmmo) {
				return;
			}
			if (Configs.NoInfAmmoTeam.Contains((int)ev.Player.GetTeam()))
			{
				return;
			}
			ev.Player.ammoBox.Networkamount = "101:101:101";
		}

		private static void HandleSetClass(SetClassEvent ev) {
			var hub = ev.Player;
			State.PrevPos[hub] = Vector3.zero;
			State.AfkTime[hub] = 0;

			if (Configs.MaxHealth.TryGetValue((int) ev.Role, out var hp)) {
				State.RunCoroutine(RunRestoreMaxHp(hub, hp));
			}
		}

		private static void HandleDroppedItem(ItemDroppedEvent ev) {
			if (Configs.ItemAutoCleanup <= 0 || Configs.ItemCleanupIgnore.Contains((int) ev.Item.ItemId)) {
				return;
			}

			State.Pickups.Add(ev.Item, State.CleanupTime + Configs.ItemAutoCleanup);
		}

		private static void HandleGeneratorUnlock(ref GeneratorUnlockEvent ev) {
			if (Configs.GeneratorKeycardPerm.Count == 0) {
				return;
			}

			ev.Allow = false;
			if (!Configs.GeneratorUnlockTeams.Contains((int) ev.Player.GetTeam())) {
				return;
			}

			var curr = ev.Player.inventory.curItem;

			var item = ev.Player.inventory.availableItems.FirstOrDefault(i => i.id == curr);
			if (item == null) {
				return;
			}

			foreach (var perm in item.permissions) {
				if (!Configs.GeneratorKeycardPerm.Contains(perm)) {
					continue;
				}

				ev.Allow = true;
				break;
			}
		}

		private static void HandleGeneratorInsert(ref GeneratorInsertTabletEvent ev) {
			if (Configs.GeneratorInsertTeams.Count == 0) {
				return;
			}

			ev.Allow = Configs.GeneratorInsertTeams.Contains((int) ev.Player.GetTeam());
		}

		private static void HandleGeneratorEject(ref GeneratorEjectTabletEvent ev) {
			if (Configs.GeneratorEjectTeams.Count == 0) {
				return;
			}

			ev.Allow = Configs.GeneratorEjectTeams.Contains((int) ev.Player.GetTeam());
		}

		private static void HandleFemurEnter(FemurEnterEvent ev) {
			if (Configs.Scp106LureAmount == -1) {
				return;
			}

			if (!Configs.Scp106LureTeam.Contains((int) ev.Player.GetTeam())) {
				ev.Allow = false;
				return;
			}

			if (++State.LuresCount < Configs.Scp106LureAmount) {
				State.RunCoroutine(RunLureReload());
			}
		}

		private static IEnumerator<float> RunLureReload() {
			yield return Timing.WaitForSeconds(Configs.Scp106LureReload);
			var lure = Object.FindObjectOfType<LureSubjectContainer>();
			lure.NetworkallowContain = false;
		}

		private static IEnumerator<float> RunForceRestart() {
			yield return Timing.WaitForSeconds(Configs.ForceRestart);

			Log.Info("Restarting round");
			PlayerManager.localPlayer.GetComponent<PlayerStats>().Roundrestart();
		}

		private static IEnumerator<float> RunAutoWarhead() {
			yield return Timing.WaitForSeconds(Configs.AutoWarheadStart);

			if (Configs.AutoWarheadLock) {
				EventPlugin.WarheadLocked = true;
			}

			if (AlphaWarheadController.Host.detonated || AlphaWarheadController.Host.NetworkinProgress) {
				Log.Info("Alpha Warhead is detonated or detonation is in progress");
				yield break;
			}

			Log.Info("Activating Alpha Warhead");
			AlphaWarheadController.Host.StartDetonation();

			if (!string.IsNullOrEmpty(Configs.AutoWarheadStartText)) {
				Map.Broadcast(Configs.AutoWarheadStartText, 10);
			}
		}

		private static IEnumerator<float> RunAutoCleanup() {
			while (true) {
				State.CleanupTime++;

				if (Configs.ItemAutoCleanup > 0) {
					foreach (var item in State.Pickups.Keys.ToArray()) {
						if (item == null) {
							State.Pickups.Remove(item);
							continue;
						}

						if (State.Pickups[item] <= State.CleanupTime) {
							NetworkServer.Destroy(item.gameObject);
						}
					}
				}

				yield return Timing.WaitForOneFrame;

				if (Configs.RagdollAutoCleanup > 0) {
					foreach (var ragdoll in State.Ragdolls.Keys.ToArray()) {
						if (ragdoll == null) {
							State.Ragdolls.Remove(ragdoll);
							continue;
						}

						if (State.Ragdolls[ragdoll] <= State.CleanupTime) {
							NetworkServer.Destroy(ragdoll.gameObject);
						}
					}
				}

				yield return Timing.WaitForSeconds(1f);
			}
		}

		private static IEnumerator<float> RunRestoreMaxHp(ReferenceHub player, int maxHp) {
			yield return Timing.WaitForSeconds(0.1f);

			player.playerStats.maxHP = maxHp;
			player.SetHealth(maxHp);
		}

		private static IEnumerator<float> RunSelfHealing() {
			while (true) {
				foreach (var hub in ReferenceHub.Hubs.Values) {
					try {
						DoSelfHealing(hub);
					}
					catch (Exception e) {
						Log.Error($"Please report this error to Nekonyx#2752: {e}");
					}

					yield return Timing.WaitForOneFrame;
				}

				yield return Timing.WaitForSeconds(1);
			}
		}

		private static void DoSelfHealing(ReferenceHub hub) {
			var role = (int) hub.GetRole();
			if (hub.characterClassManager.IsHost || !Configs.SelfHealingAmount.TryGetValue(role, out var amount) ||
			    !Configs.SelfHealingDuration.TryGetValue(role, out var duration)) {
				return;
			}

			var pos = hub.GetPosition();

			State.AfkTime[hub] = State.PrevPos[hub] == pos ? State.AfkTime[hub] + 1 : 0;
			State.PrevPos[hub] = pos;

			if (State.AfkTime[hub] <= duration) {
				return;
			}

			var maxHp = GetMaxHp(hub);
			var nextHp = hub.GetHealth() + amount;
			hub.SetHealth(nextHp >= maxHp ? maxHp : nextHp);
		}

		private static int GetMaxHp(ReferenceHub player) =>
			Configs.MaxHealth.TryGetValue((int) player.GetRole(), out var hp) ? hp : player.playerStats.maxHP;
	}
}
